class PostsController < ApplicationController
  def index
    @posts = Posts.all
    render json: @posts
  end

  def show
    @post = Post.last
    render json: @post
  end

  def create
  end

  def edit
  end

  def new
  end

  def destroy
  end
end
